#include "readhdwareinfo.h"

readHdwareInfo::readHdwareInfo()
{

}

bool readHdwareInfo::getBoard()
{
    FILE * hdFd = NULL;
    char buff[512] = {0};
    int  i = 0, j = 0;
    char *result = NULL;
    if(NULL == (hdFd = fopen(HDPATH,"rb"))) {
        printf("fopen hardware_info error\n");
        return false;
    }
    memset(buff,0,sizeof(buff));
    while(fread(buff+i,1,1,hdFd) != 0) {
        i++;
    }
    printf("hardware_info:\n%s",buff);

    result = strstr(buff,splt);
    if(result != NULL) {
        i = 0;
        result += strlen(splt);
        while((result[i] != '\"') && (result[i] != '\n'))
            i++;
        if(result[i] == '\n') {
            printf("not get board info\n");
            return false;
        }

        memset(board,0,sizeof(board));
        i++;
        while((result[i] != '\"') && (result[i] != '\n')) {
            board[j++] = result[i++];
        }
        if(result[i] == '\n') {
            printf("not get board end\n");
            return false;
        }
        else {
            printf("board:\033[1m\033[45;33m%s\033[0m\n",board);
            memset(iniPath,0,sizeof(iniPath));
            sprintf(iniPath,"/data/%s.ini",board);
        }
    }
    else {
        printf("not find splt(%s)\n",splt);
        return false;
    }
    fclose(hdFd);
    return true;
}


bool readHdwareInfo::iniGlobal(KVMINFO *info_)
{
    memset(info_,0,sizeof(KVMINFO));
    FILE * iniFd = NULL;
    char *result = NULL;
    char *spltIn = "kvm_in";
    char *spltOut = "kvm_out";
    char buff[512] = {0};
    int i = 0, j = 0;

    if(NULL == (iniFd = fopen(iniPath,"rb"))) {
        printf("fopen board ini error\n");
        return false;
    }
    memset(buff,0,sizeof(buff));
    while(fread(buff+i,1,1,iniFd) != 0) {
        i++;
    }
    i = 0;
    j = 0;
    result = strstr(buff,spltIn);
    if(result !=  NULL) {
        result += (strlen(spltIn) + 1);
        while((result[i] != '\n') && (result[i] != 0)) {
            info_->kvm_in[j++] = result[i];
            i++;
        }
    }
    else {
        printf("not get kvm_in info\n");
        return false;
    }

    i = 0;
    j = 0;
    result = strstr(buff,spltOut);
    if(result !=  NULL) {
        result += (strlen(spltOut) + 1);
        strncpy(info_->kvm_out,"/dev/input/",strlen("/dev/input/"));
        j = strlen("/dev/input/");
        while((result[i] != '\n') && (result[i] != 0)) {
            info_->kvm_out[j++] = result[i];
            i++;
        }

    }
    else {
        printf("not get kvm_out info\n");
        return false;
    }

    printf("kvm_in:\033[1m\033[45;33m%s\033[0m\n",info_->kvm_in);
    printf("kvm_in:\033[1m\033[45;33m%s\033[0m\n",info_->kvm_out);

    return true;
}
