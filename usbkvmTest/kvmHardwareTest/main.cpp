#include <iostream>
#include "readhdwareinfo.h"
#include <common.h>
#include "./common/singleton.h"
#include "global.h"

using namespace std;

#define PORT 16333
#define KEYDATA 11


bool getInfo(char *src,char *data)
{
    char *splt = "=";
    char *result  = NULL;
    int i = 0, j = 0;

    result = strstr(src,splt);
    if(result != NULL) {
        result += 1;
        while((result[i] != '\n') && result[i]) {
            data[j++] = result[i++];
        }
    }
    else {
        printf("rcv data error\n");
        return false;
    }
    return true;
}

bool getKeyValue(char key,char *value,bool flag)
{
    value[0] = 0x57;
    value[1] = 0xAB;
    value[2] = 0x01;
    if(flag) {
        value[5] = 0x0;
        return true;
    }
    switch(key) {
        case 'A':
            value[5] = 0x04;
            break;
        case 'B':
            value[5] = 0x05;
            break;
        case 'C':
            value[5] = 0x06;
            break;
        case 'D':
            value[5] = 0x07;
            break;
        case 'E':
            value[5] = 0x08;
            break;
        case 'F':
            value[5] = 0x09;
            break;
        case 'G':
            value[5] = 0x0A;
            break;
        case 'H':
            value[5] = 0x0B;
            break;
        case 'I':
            value[5] = 0x0C;
            break;
        case 'J':
            value[5] = 0x0D;
            break;
        case 'K':
            value[5] = 0x0E;
            break;
        case 'L':
            value[5] = 0x0F;
            break;
        case 'M':
            value[5] = 0x10;
            break;
        case 'N':
            value[5] = 0x11;
            break;
        case 'O':
            value[5] = 0x12;
            break;
        case 'P':
            value[5] = 0x13;
            break;
        case 'Q':
            value[5] = 0x14;
            break;
        case 'R':
            value[5] = 0x15;
            break;
        case 'S':
            value[5] = 0x16;
            break;
        case 'T':
            value[5] = 0x17;
            break;
        case 'U':
            value[5] = 0x18;
            break;
        case 'V':
            value[5] = 0x19;
            break;
        case 'W':
            value[5] = 0x1A;
            break;
        case 'X':
            value[5] = 0x1B;
            break;
        case 'Y':
            value[5] = 0x1C;
            break;
        case 'Z':
            value[5] = 0x1D;
            break;
        default:
            break;
    }
    return true;
}

bool a2A(char *data)
{
    int i = 0;
    for(i = 0; i < strlen(data); i++) {
        if(data[i] >= 97) {
            data[i] = data[i] - 32;
        }
    }
    return true;
}


void * TestWrite(void * info)
{
    printf("start write\n");
    TESTINFO testInfo = *(TESTINFO*)info;
    int i = 0, j = 0, n = 0;
    char keyData[64][KEYDATA] = {0};
    auto myserial = Singleton<MySerial>::getInstance();

/*
 * 根据字符生成对应的键盘值（按下和抬起）
 * */

    for(i = 0; i < strlen(testInfo.buff_); i++) {
        memset(keyData[2*i],0,KEYDATA);
        getKeyValue(testInfo.buff_[i],keyData[2*i],false);
        memset(keyData[2*i+1],0,KEYDATA);
        getKeyValue(testInfo.buff_[i],keyData[2*i+1],true);
    }

//    for(i = 0; i < strlen(testInfo.buff_)*2; i++) {
//        for(j = 0; j < KEYDATA; j++)
//            printf("0x%x ",keyData[i][j]);
//        printf("\n");
//    }

/*
 * 向UART写入数据
 * */

    for(i = 0; i < testInfo.number_; i++) {
        for(j = 0; j < strlen(testInfo.buff_);j++) {
            myserial->uartWrite(keyData[2*j],KEYDATA);
            usleep(1000*testInfo.delay_);
//            for(n = 0; n < KEYDATA; n++)
//                printf("0x%x ",keyData[2*j][n]);
//            printf("\n");
            myserial->uartWrite(keyData[2*j+1],KEYDATA);
//            for(n = 0; n < KEYDATA; n++)
//                printf("0x%x ",keyData[2*j+1][n]);
//            printf("\n");

        }
    }
    printf("=====================\n");
    printf("uart write end:%d\n",i);
    printf("=====================\n");

    return (void*)0;
}

pthread_t CreateThread(void* (*pThreadFunc)(void*), uint priority, int schedpolicy, bool b_detached, void *threadin)
{
    pthread_t tid;
    pthread_attr_t attr;
    struct sched_param  param;
    pthread_attr_init(&attr);
    param.sched_priority = priority; //优先级：1～99
    //pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);//要使优先级其作用必须要有这句话.表示使用在schedpolicy和schedparam属性中显式设置的调度策略和参数
    pthread_attr_setschedparam(&attr, &param);
    pthread_attr_setschedpolicy(&attr, schedpolicy);

    if(b_detached)
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED); //分离模式，无需的等待（pthread_join）之后才释放占用资源，而是返回后自动释放占用的资源

    if(pthread_create(&tid, &attr, pThreadFunc, threadin) < 0){
        return 0;
    }

    pthread_attr_destroy(&attr);

    //pthread_timedjoin_np(); //超时等待
    return tid;
}

int main()
{
    KVMINFO kvminfo;
    TESTINFO testInfo;
    char rcvdata[64] = {0};
    char echoData[32] = {0};
    char rdbuff[KEYDATA] = {0};
    char tmp[8] = {0};
    struct input_event keydata;


    int  n = 0, rdlen = 0;
    int outFd = 0;


    int len = 0;
    int qNum = 0;


/*
 * 根据 /version/hareware_info 中board值 读取 /data/下对应的硬件ini信息，配置KVM信息
 * */

    auto ini =  Singleton<readHdwareInfo>::getInstance();
    ini->getBoard();
    ini->iniGlobal(&kvminfo);

    UDPSocket *udpsocket = new UDPSocket();
    udpsocket->CreateUDPServer(PORT, false);

/*
 * 从UMP软件获取测试英文字符（自动转化大写），延迟时间，测试次数
 * */

    memset(&testInfo,0,sizeof(TESTINFO));
    while( qNum != 3 ){
        memset(rcvdata,0,sizeof(rcvdata));
        if( (len = udpsocket->RecvFrom(rcvdata, 256)) > 0) {
            if(strstr(rcvdata,"detail")) {
                getInfo(rcvdata,testInfo.buff_);
                printf("detail:\033[1m\033[45;33m%s\033[0m\n",testInfo.buff_);
                a2A(testInfo.buff_);
                qNum++;
            }
            else if(strstr(rcvdata,"delay")) {
                memset(tmp,0,sizeof(tmp));
                getInfo(rcvdata,tmp);
                testInfo.delay_ = atoi(tmp);
                printf("delay:\033[1m\033[45;33m%d\033[0m\n",testInfo.delay_);
                qNum++;

            }
            else if(strstr(rcvdata,"number")) {
                memset(tmp,0,sizeof(tmp));
                getInfo(rcvdata,tmp);
                testInfo.number_ = atoi(tmp);
                printf("number:\033[1m\033[45;33m%d\033[0m\n",testInfo.number_);
                qNum++;

            }
            else {
                printf("rcv error data\n");
            }
        }
        else{
            printf("no data from client\n");
        }
    }



/*
 * 打开KVM硬件测试输入端串口，设置默认属性115200，8，N，1
 *
 * */

    auto myserial = Singleton<MySerial>::getInstance();

    if( !strcmp(kvminfo.kvm_in,"COM0") ){
        // 打开 UART0 串口进行读写
        if(myserial->open_port( MySerial::kUART0 )  == -1){
            printf("open serial port 0 failed!\n");
            return 0;
        }
        printf("uart0 open\n");
    }
    else if( !strcmp(kvminfo.kvm_in,"COM1") ){
        // 打开 UART1 串口进行读写
        if(myserial->open_port( MySerial::kUART1 )  == -1){
            printf("open serial port 1 failed!\n");
            return 0;
        }
        printf("uart1 open\n");
    }
    else if( !strcmp(kvminfo.kvm_in,"COM2") ){
        // 打开 UART2 串口进行读写

        if(myserial->open_port( MySerial::kUART2 )  == -1){
            printf("open serial port 2 failed!\n");
            return 0;
        }
        printf("uart2 open\n");
    }
    else if( !strcmp(kvminfo.kvm_in,"COM3") ){
        // 打开 UART3 串口进行读写

        if(myserial->open_port( MySerial::KUART3 )  == -1){
            printf("open serial port 3 failed!\n");
            return 0;
        }
        printf("uart3 open\n");
    }

    if( myserial->set_opt(115200,8,1,'N',3,10)< 0){
        myserial->close_port();
        printf("serial set opt failed!\n");
        return 0;
    }

/*
 * 开启线程，串口write健值
 *
 * */
    bool b_detached = true; //设置线程为分离模式
    pthread_t kvmIn_tid = CreateThread(TestWrite, 0, SCHED_FIFO, b_detached, &testInfo);
    if(kvmIn_tid == 0){
        printf("pthread_create() failed: kvm in\n");
    }

//    while(1) {
//        sleep(1000);
//    }



/*
* 读取KVMOUT，并向UMP返回读取次数，以做判断是否有丢健值
*
* */
    if((outFd = open(kvminfo.kvm_out,O_RDONLY)) < 0) {
        printf("open kvm out error\n");
        return 0;
    }
    while(n < testInfo.number_ * 2) {
        rdlen = read(outFd,&keydata,sizeof(struct input_event));
        if(rdlen == sizeof(struct input_event)) {
            n++;
            printf("read data-->%d\n",n);
            if(n%2 == 0) {
                memset(echoData,0,sizeof(echoData));
                sprintf(echoData,"readNumber=%d",n/2);
                udpsocket->EchoTo(echoData,strlen(echoData));
            }

        }
    }

    printf("test KVM end\n");
    return 1;

}
