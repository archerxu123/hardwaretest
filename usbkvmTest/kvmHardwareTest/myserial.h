#ifndef MYSERIAL_H
#define MYSERIAL_H

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/mman.h>

#include "global.h"
#include <mpi_sys.h>

#define UART3_DR 0x12130000
#define UART3_FR 0x12130018
#define UART3_LCR_H 0x1213002C

#define ASC2NUM(ch) (ch - '0')
#define HEXASC2NUM(ch) (ch - 'A' + 10)

#define PAGE_SIZE       0x1000
#define PAGE_SIZE_MASK  0xfffff000
#define DEFAULT_MD_LEN  128

#define CFGSIZE   512

#define MAP_FAILED	((void *) -1)

#define PROT_READ	0x1		/* Page can be read.  */
#define PROT_WRITE	0x2		/* Page can be written.  */
#define PROT_EXEC	0x4		/* Page can be executed.  */
#define PROT_NONE	0x0		/* Page can not be accessed.  */
#define PROT_GROWSDOWN	0x01000000	/* Extend change to start of
                       growsdown vma (mprotect only).  */
#define PROT_GROWSUP	0x02000000	/* Extend change to start of
                       growsup vma (mprotect only).  */
#define MAP_SHARED	0x01		/* Share changes.  */
#define MAP_PRIVATE	0x02		/* Changes are private.  */

typedef struct tag_MMAP_Node
{
    unsigned int Start_P;
    unsigned int Start_V;
    unsigned int length;
    unsigned int refcount;  /* map后的空间段的引用计数 */
    struct tag_MMAP_Node * next;
}TMMAP_Node_t;



class MySerial
{
public:
    typedef enum {
        kUART0 = 0,
        kUART1,
        kUART2,
        KUART3,
        kCOMPortMax,
    }ComPort;

    MySerial();
    ~MySerial();
public:
    static bool set_opt(int nSpeed, int nBits, int nStop, char nEvent, int nVtime, int nVmin);
    static bool open_port(ComPort comport);
    static void close_port();
    static int nwrite(const char *data, int datalength);
    static int nread(char *data, int datalength);
    static int uartWrite(char *data,int len);

private:
    static int fd_;
};

#endif // MYSERIAL_H
