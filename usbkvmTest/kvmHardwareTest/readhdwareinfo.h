#ifndef READHDWAREINFO_H
#define READHDWAREINFO_H

#include <stdio.h>
#include <string.h>
#include "global.h"

#define HDPATH "/version/hardware_info"


class readHdwareInfo
{
public:
    readHdwareInfo();
    bool getBoard();
    bool iniGlobal(KVMINFO *info_);
private:
    char board[64] = {0};
    char iniPath[64] = {0};
    char *splt = "\"board\"";
};

#endif // READHDWAREINFO_H
