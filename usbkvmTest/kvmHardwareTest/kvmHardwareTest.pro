TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

##指定目标文件(obj)的存放目录
OBJECTS_DIR += ../obj
HISI_SDK_VER = /home/jhxu/work/hi_sdk/hi3531d_050/Hi3531DV100_SDK_V1.0.5.0/mpp

#头文件包含路径
INCLUDEPATH +=  /home/jhxu/work/gitclone/OLED/usbkvmTest/kvmHardwareTest/common \
                /home/jhxu/work/hi_sdk/hi3531d_050/Hi3531DV100_SDK_V1.0.5.0/mpp/include


LIBS += -lpthread -lm -ldl -lrt \
        -lstdc++

#不检测定义后未使用的参数错误等
QMAKE_CXXFLAGS += -Wno-unused-parameter \
                  -Wno-unused-but-set-variable \
                  -Wno-unused-but-set-parameter \
                  -Wno-literal-suffix
                  -std=c++11
QMAKE_CFLAGS += -Wno-unused-parameter \
                -Wno-unused-but-set-variable \
                -Wno-unused-but-set-parameter

LIBS += $${HISI_SDK_VER}/lib/libmpi.a \
        $${HISI_SDK_VER}/lib/libhdmi.a \
        $${HISI_SDK_VER}/lib/libive.a \
        $${HISI_SDK_VER}/lib/libjpeg6b.a \
        $${HISI_SDK_VER}/lib/libjpeg.a \
        $${HISI_SDK_VER}/lib/libpciv.a \
        $${HISI_SDK_VER}/lib/libtde.a \
#        $${HISI_SDK_VER}/lib/libaacenc.a \
        $${HISI_SDK_VER}/lib/libaacdec.a \
        $${HISI_SDK_VER}/lib/libVoiceEngine.a \
        $${HISI_SDK_VER}/lib/libupvqe.a \
        $${HISI_SDK_VER}/lib/libdnvqe.a

SOURCES += main.cpp \
    udpsocket/udpsocket.cpp \
    readhdwareinfo.cpp \
    myserial.cpp

HEADERS += \
    readini.h \
    global.h \
    udpsocket/udpsocket.h \
    udpsocket/networkinfo.h \
    readhdwareinfo.h \
    common/common.h \
    myserial.h
