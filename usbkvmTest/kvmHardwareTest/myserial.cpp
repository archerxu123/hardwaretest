#include "myserial.h"

int MySerial::fd_ = -1;

MySerial::MySerial()
{
}

MySerial::~MySerial()
{
}

/*
 * 功能： 设置串口参数
 * 输入： int nSpeed：波特率
 *       int nBits： 数据位，支持 - 7， 8
 *       char nEvent： 校验位，支持 - 偶'O', 奇'E', 无'N'
 *       int nStop： 停止位，支持 - 1， 2
 *       int nVtime： 串口read经过 nVtime 个0.1秒后返回
 *       int nVmin： 串口read的等待有 nVmin 个字符后进行读取
 * 返回： bool
 * 日期： 2018.07.07
 * 作者： zh.sun
 */
bool MySerial::set_opt(int nSpeed, int nBits, int nStop, char nEvent, int nVtime, int nVmin)  //串口设置
{
    struct termios newtio, oldtio;
    if  ( tcgetattr( fd_, &oldtio)  !=  0) {
        perror("SetupSerial");
        return false;
    }
    bzero( &newtio, sizeof( newtio ) );
    newtio.c_cflag  |=  CLOCAL | CREAD;
    newtio.c_cflag &= ~CSIZE;

    //	newtio.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    //	newtio.c_oflag &= ~OPOST;
    switch( nBits )
    {
        case 7:
            newtio.c_cflag |= CS7;
            break;
        case 8:
            newtio.c_cflag |= CS8;
            break;
    }

    switch( nEvent )
    {
        case 'O':
            newtio.c_cflag |= PARENB;
            newtio.c_cflag |= PARODD;
            newtio.c_iflag |= (INPCK | ISTRIP);
            break;
        case 'E':
            newtio.c_iflag |= (INPCK | ISTRIP);
            newtio.c_cflag |= PARENB;
            newtio.c_cflag &= ~PARODD;
            break;
        case 'N':
            newtio.c_cflag &= ~PARENB;
            break;
    }

    switch( nSpeed )
    {
        case 2400:
            cfsetispeed(&newtio, B2400);
            cfsetospeed(&newtio, B2400);
            break;
        case 4800:
            cfsetispeed(&newtio, B4800);
            cfsetospeed(&newtio, B4800);
            break;
        case 9600:
            cfsetispeed(&newtio, B9600);
            cfsetospeed(&newtio, B9600);
            break;
        case 38400:
            cfsetispeed(&newtio, B38400);
            cfsetospeed(&newtio, B38400);
            break;
        case 115200:
            cfsetispeed(&newtio, B115200);
            cfsetospeed(&newtio, B115200);
            break;
        case 57600:
            cfsetispeed(&newtio, B57600);
            cfsetospeed(&newtio, B57600);
            break;
        default:
            cfsetispeed(&newtio, B9600);
            cfsetospeed(&newtio, B9600);
            break;
    }

    if( nStop == 1 )
        newtio.c_cflag &=  ~CSTOPB;
    else if ( nStop == 2 )
        newtio.c_cflag |=  CSTOPB;

    /*
    TIME和MIN值
    这两个值只用于非标准模式,两者结合共同控制对输入的读取方式,还能控制在一个程序试图与一个终端关联的文件描述符时将发生的情况
    MIN = 0, TIME = 0时:read立即返回,如果有待处理的字符,它们就会被返回,如果没有,read调用返回0,且不读取任何字符
    MIN = 0, TIME > 0时:有字符处理或经过TIME个0.1秒后返回
    MIN > 0, TIME = 0时:read一直等待,直到有ＭＩＮ个字符可以读取,返回值是字符的数量.到达文件尾时返回0
    MIN > 0, TIME > 0时:read调用时,它会等待接收一个字符.在接收到第一个字符及其后续的每个字符后,启用一个字符间隔定时器.当有ＭＩＮ个字符可读或两字符间的时间间隔超进TIME个0.1秒时,read返回
    通过设置ＭＩＮ和ＴＩＭＥ值,我们可以逐个字符地对输入进行处理
    */
    newtio.c_cc[VTIME]  = nVtime;
    newtio.c_cc[VMIN] = nVmin;
    tcflush(fd_,TCOFLUSH);
    if((tcsetattr(fd_, TCSADRAIN, &newtio))!=0)
    {
        perror("com set tcsetattr error");
        return false;
    }
    return true;
}

TMMAP_Node_t *pTMMAPNode = NULL;

int atoul(const char *str,unsigned int * pulValue)
{
    unsigned int ulResult=0;

    while (*str)
    {
        if (isdigit((int)*str))
        {
            /*最大支持到0xFFFFFFFF(4294967295),
               X * 10 + (*str)-48 <= 4294967295
               所以， X = 429496729 */
            if ((ulResult<429496729) || ((ulResult==429496729) && (*str<'6')))
            {
                ulResult = ulResult*10 + (*str)-48;
            }
            else
            {
                *pulValue = ulResult;
                return -1;
            }
        }
        else
        {
            *pulValue=ulResult;
            return -1;
        }
        str++;
    }
    *pulValue=ulResult;
    return 0;
}

int atoulx(const char *str, unsigned int * pulValue)
{
    unsigned int   ulResult=0;
    unsigned char ch;

    while (*str)
    {
        ch=toupper(*str);
        if (isdigit(ch) || ((ch >= 'A') && (ch <= 'F' )))
        {
            if (ulResult < 0x10000000)
            {
                ulResult = (ulResult << 4) + ((ch<='9')?(ASC2NUM(ch)):(HEXASC2NUM(ch)));
            }
            else
            {
                *pulValue=ulResult;
                return -1;
            }
        }
        else
        {
            *pulValue=ulResult;
            return -1;
        }
        str++;
    }

    *pulValue=ulResult;
    return 0;
}

void *memmap(unsigned int phy_addr, unsigned int size)
{
    static int fd = -1;
    static const char dev[]="/dev/mem";

    unsigned int phy_addr_in_page;
    unsigned int page_diff;
    unsigned int size_in_page;

    TMMAP_Node_t * pTmp;
    TMMAP_Node_t * pNew;

    void *addr=NULL;

    if(size == 0)
    {
        printf("memmap():size can't be zero!\n");
        return NULL;
    }

    /* check if the physical memory space have been mmaped */
    pTmp = pTMMAPNode;
    while(pTmp != NULL)
    {
        if( (phy_addr >= pTmp->Start_P) &&
            ( (phy_addr + size) <= (pTmp->Start_P + pTmp->length) ) )
        {
            pTmp->refcount++;   /* referrence count increase by 1  */
            return (void *)(pTmp->Start_V + phy_addr - pTmp->Start_P);
        }

        pTmp = pTmp->next;
    }

    /* not mmaped yet */
    if(fd < 0)
    {
        /* dev not opened yet, so open it */
        fd = open (dev, O_RDWR | O_SYNC);
        if (fd < 0)
        {
            printf("memmap():open %s error!\n", dev);
            return NULL;
        }
    }

    /* addr align in page_size(4K) */
    phy_addr_in_page = phy_addr & PAGE_SIZE_MASK;
    page_diff = phy_addr - phy_addr_in_page;

    /* size in page_size */
    size_in_page =((size + page_diff - 1) & PAGE_SIZE_MASK) + PAGE_SIZE;

    addr = mmap ((void *)0, size_in_page, PROT_READ|PROT_WRITE, MAP_SHARED, fd, phy_addr_in_page);
    if (addr == MAP_FAILED)
    {
        printf("memmap():mmap @ 0x%x error!\n", phy_addr_in_page);
        return NULL;
    }

    /* add this mmap to MMAP Node */
    pNew = (TMMAP_Node_t *)malloc(sizeof(TMMAP_Node_t));
    if(NULL == pNew)
    {
        printf("memmap():malloc new node failed!\n");
        return NULL;
    }
    pNew->Start_P = phy_addr_in_page;
    pNew->Start_V = (unsigned int)addr;
    pNew->length = size_in_page;
    pNew->refcount = 1;
    pNew->next = NULL;

    if(pTMMAPNode == NULL)
    {
        pTMMAPNode = pNew;
    }
    else
    {
        pTmp = pTMMAPNode;
        while(pTmp->next != NULL)
        {
            pTmp = pTmp->next;
        }

        pTmp->next = pNew;
    }

    return (void *)((unsigned int)addr+page_diff);
}

int StrToNumber( const char *str ,  unsigned int * pulValue)
{
    /*判断是否16进制的字符串*/
    if ( *str == '0' && (*(str+1) == 'x' || *(str+1) == 'X') )
    {
        if (*(str+2) == '\0')
        {
            return -1;
        }
        else
        {
            return atoulx(str+2,pulValue);
        }
    }
    else
    {
        return atoul(str,pulValue);
    }
}

void* himm(const char* addr, const char* bit)
{
    unsigned int ulOld, ulNew;
    unsigned int ulAddr = 0;
    void *pMem = NULL;

    if(bit == NULL){
        if( StrToNumber(addr, &ulAddr) == 0){
            pMem = memmap(ulAddr, DEFAULT_MD_LEN);
            //printf("%s: 0x%08lX\n", addr, *(U32*)pMem);
            return pMem;
        }
        else{
            printf("ERROR:Please input addr like 0x12345678\n");
        }
    }
    else if( StrToNumber(addr, &ulAddr) == 0 &&
             StrToNumber(bit, &ulNew) == 0){
        pMem = memmap(ulAddr, DEFAULT_MD_LEN);
        ulOld = *(unsigned int*)pMem;
        *(unsigned int*)pMem = ulNew;
        //printf("%s: 0x%08lX --> 0x%08lX \n", addr, ulOld, ulNew);
        return (void*)ulNew;
    }
    else{
        printf("ERROR:xxx\n");
    }

    return NULL;
}


HI_S32 SAMPLE_SYS_GetReg(HI_U32 u32Addr, HI_U32 *pu32Value)
{
    HI_U32 *pu32Addr = NULL;
    HI_U32 u32MapLen;

    if (NULL == pu32Value)
    {
        return HI_ERR_SYS_NULL_PTR;
    }

    u32MapLen = sizeof(*pu32Value);
    pu32Addr = (HI_U32 *)HI_MPI_SYS_Mmap(u32Addr, u32MapLen);
    if(NULL == pu32Addr)
    {
        return HI_FAILURE;
    }

    *pu32Value = *pu32Addr;

    return HI_MPI_SYS_Munmap(pu32Addr, u32MapLen);
}

HI_S32 SAMPLE_SYS_SetReg(HI_U32 u32Addr, HI_U32 u32Value)
{
    HI_U32 *pu32Addr = NULL;
    HI_U32 u32MapLen = sizeof(u32Value);

    pu32Addr = (HI_U32 *)HI_MPI_SYS_Mmap(u32Addr, u32MapLen);
    if(NULL == pu32Addr)
    {
        return HI_FAILURE;
    }

    *pu32Addr = u32Value;

    return HI_MPI_SYS_Munmap(pu32Addr, u32MapLen);
}

void delaytime_(unsigned int tmp)
{
    unsigned long t = tmp *100000;
    while(t)
        t--;
}


int MySerial::uartWrite(char *data,int len)
{
    HI_U32 lcrData = 0;
    HI_U32 frTx5 = 0 ;
    HI_U32 frTx7 = 0;
    int uartdata = 0;
    HI_U32 value  = 0;
    HI_U32 uartSt = 0;
    int i = 0;
//    lcrData = *(int*)himm(UART3_LCR_H,NULL);
//    printf("uart lcr_h fen--->0x%x\n",(lcrData&0x10)>>4);
//    frTx5 = ((*(int*)himm(UART3_FR,NULL))&0x10)>>4;
    SAMPLE_SYS_GetReg(UART3_FR,&frTx5);

    for(i = 0; i < len; i++) {
//        uartSt = ((*(int*)himm(UART3_FR,NULL))&0x8)>>3;
//        if(uartSt == 1)
//            printf("uart busy\n");
//        else
//            printf("uart empty\n");

        while(frTx5 & 0x20) {
            SAMPLE_SYS_GetReg(UART3_FR,&frTx5);
//            frTx5 = ((*(int*)himm(UART3_FR,NULL))&0x20)>>4;
        }
//        printf("FR[5]--->0x%x\n",frTx5);

        SAMPLE_SYS_GetReg(UART3_FR,&frTx5);
        while(frTx5 & 0x8)
            SAMPLE_SYS_GetReg(UART3_FR,&frTx5);
//        printf("FR[5]--->0x%x\n",frTx5);

//        while(1) {

//        }

//        uartdata = *(int*)himm(UART3_DR,NULL);
//        uartdata |= data[i];
//        memset(value,0,16);
//        sprintf(value,"0x%x",uartdata);
//        printf("data-->%s\n",value);

//        himm(UART3_DR,value);

        value = (HI_U32)data[i];
//        printf("data->0x%x",value);
        SAMPLE_SYS_SetReg(UART3_DR,value);
        delaytime_(1);

        SAMPLE_SYS_GetReg(UART3_FR,&frTx7);
//        frTx7 = ((*(int*)himm(UART3_FR,NULL))&0x80)>>7;
        while(!(frTx7&0x80)) {
            SAMPLE_SYS_GetReg(UART3_FR,&frTx7);
//            frTx7 = ((*(int*)himm(UART3_FR,NULL))&0x80)>>7;
        }
//        printf("FR[7]--->0x%d\n",frTx7);


    }




    return 1;
}

bool MySerial::open_port(ComPort comport) //通过参数，打开相应的串口
{
    if (comport == kUART0)
    {	fd_ = open( "/dev/ttyAMA0", O_RDWR|O_NOCTTY|O_NDELAY);
        if (-1 == fd_){
            perror("Can't Open Serial UART0 Port");
            return false;
        }
    }
    else if(comport == kUART1)
    {	fd_ = open( "/dev/ttyAMA1", O_RDWR|O_NOCTTY|O_NDELAY);
        if (-1 == fd_){
            perror("Can't Open Serial UART1 Port");
            return false;
        }
    }
    else if (comport == kUART2)
    {
        fd_ = open( "/dev/ttyAMA2", O_RDWR|O_NOCTTY|O_NDELAY);
        if (-1 == fd_){
            perror("Can't Open Serial UART2 Port");
            return false;
        }
    }
    else if (comport == KUART3)
    {
        fd_ = open("/dev/ttyAMA3", O_WRONLY | O_NOCTTY | O_SYNC);
        if( -1 == fd_) {
            perror("Can't Open Serial UART3 Port\n");
            return false;
        }
    }

    if(fcntl(fd_, F_SETFL, 0)<0){
        perror("fcntl failed!");
        return false;
    }
    if(isatty(STDIN_FILENO)==0){
        perror("standard input is not a terminal device");
        return false;
    }

    return true;
}

void MySerial::close_port()
{
    if(fd_ > 0)
        close(fd_);
}

int MySerial::nwrite(const char *data, int datalength )  //写串口信息
{
    int len = 0, total_len = 0;

    for (total_len = 0 ; total_len < datalength;){
        len = 0;
        len = write(fd_, &data[total_len], datalength - total_len);
        if (len > 0)
            total_len += len;
    }

    return total_len;
}

int MySerial::nread(char *data, int datalength)   //读取串口信息
{
    int readlen = 0;
    if((readlen = read(fd_,data,datalength)) > 0){
        printf("com read is %s\n",data);
    }
    else{
        printf("com read failed!\n");
        return -1;
    }

    return readlen;
}

