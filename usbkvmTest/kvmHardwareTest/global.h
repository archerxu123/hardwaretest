#ifndef GLOBAL_H
#define GLOBAL_H

#include <singleton.h>
#include <common.h>
#include <map>

#include "udpsocket/udpsocket.h"
#include "myserial.h"



typedef struct kvmInfo
{
    char kvm_in[16];
    char kvm_out[16];
}KVMINFO;

typedef struct testInfo
{
    char buff_[32];
    int delay_;
    int number_;
}TESTINFO;

typedef unsigned short __u16;
typedef  int __s32;

struct input_event {
    struct timeval time;
    __u16 type;
    __u16 code;
    __s32 value;
};








#endif // GLOBAL_H
